Source: wig
Section: misc
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Samuel Henrique <samueloph@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python, python3-all
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/jekyc/wig
Vcs-Git: https://salsa.debian.org/pkg-security-team/wig.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/wig

Package: wig
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: WebApp Information Gatherer
 This package contains a web application information gathering tool, which can
 identify numerous Content Management Systems and other administrative
 applications.
 .
 The application fingerprinting is based on checksums and string matching of
 known files for different versions of CMSes. This results in a score being
 calculated for each detected CMS and its versions. Each detected CMS is
 displayed along with the most probable version(s) of it. The score calculation
 is based on weights and the amount of "hits" for a given checksum.
 .
 wig also tries to guess the operating system on the server based on the
 'server' and 'x-powered-by' headers. A database containing known header values
 for different operating systems is included in wig, which allows wig to guess
 Microsoft Windows versions and Linux distribution and version.
